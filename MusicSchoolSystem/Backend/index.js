const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const mysql = require('mysql')

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'student',
    password :'student',
    database : 'MusicSchoolSystem'
});

connection.connect();

const express = require('express')
const app = express()
const port = 4000 

/*API INSERT Student*/
app.post("/add_student", (req, res) => {

    let student_name = req.query.student_name
    let student_surname = req.query.student_surname
    let student_username =  req.query.student_username
    let student_password =  req.query.student_password

    bcrypt.hash(student_password, SALT_ROUNDS, (err, hash) => {
            let query = `INSERT INTO Student
                    ( StudentName, StudentSurname, Username, Password, IsAdmin)
                    VALUES ('${student_name}',
                            '${student_surname}',
                            '${student_username}','${hash}',false)`
            console.log(query)
            connection.query( query, (err, rows) => {
                if (err) {
                    res.json({
                            "status" : "400" ,
                            "message" : "Error to insert student"
                            })
                }else { 
                    res.json({
                        "status" : "200",
                        "message" : "Adding student succesful"
                    })
            }
        });
    })       
});

/*API SELECT Student*/
app.get("/list_student",(req, res) => {
    let query = "SELECT * FROM Student";
    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                    "status" : "400" ,
                    "message" : "Error querying from student db"
                    })
        }else { 
            res.json(rows)
        }
    });
})

/*API Updete Student*/
app.post("/update_student", (req, res) => {

    let student_id = req.query.student_id
    let student_name = req.query.student_name
    let student_surname = req.query.student_surname
    
    let query = `UPDATE Student SET
                    StudentName='${student_name}',
                    StudentSurname='${student_surname}'
                    WHERE StudentID=${student_id}`
    console.log(query)
    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                    "status" : "400" ,
                    "message" : "Error to update student"
                    })
        }else { 
            res.json({
                "status" : "200",
                "message" : "Update student succesful"
              })
        }
    });
});

/*API Delete Student*/
app.post("/delete_student", (req, res) => {

    let student_id = req.query.student_id
    
    let query = `DELETE FROM Student WHERE StudentID=${student_id}`
                    
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                    "status" : "400" ,
                    "message" : "Error deleting record"
                    })
        }else { 
            res.json({
                "status" : "200",
                "message" : "Deleting record success"
              })
        }
    });
})

app.listen(port, () => {
    console.log(`Now starting Running System Backend ${port}`)
})

/*query = "SELECT * from Student";
connection.query( query, (err, rows) => {
    if(err) {
        console.log(err);
    }else{
        console.log(rows); 
    }
});

connection.end();*/